﻿using Platform.Commands;
using System.Threading.Tasks;

namespace Platform.Application
{
    public interface IAccountAppService 
    {
        Task AddAccount(AddAccountCommand command);

        Task UpdateAccountBalance(UpdateAccountCommand command);
    }
}
