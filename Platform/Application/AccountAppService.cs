﻿using Platform.Commands;
using Platform.Domain.Accounts;
using Platform.Infrastructure.Database;
using System;
using System.Threading.Tasks;

namespace Platform.Application
{
    public class AccountAppService : IAccountAppService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountAppService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        public async Task AddAccount(AddAccountCommand command)
        {
            var account = new Account
            {
                Balance = command.Balance,
                BankId = command.BankId,
                Type = command.Type
            };

            _unitOfWork.Account.CreateAccount(account);

            await _unitOfWork.CommitAsync();
        }

        public async Task UpdateAccountBalance(UpdateAccountCommand command)
        {
            var account = await _unitOfWork.Account.GetAccountById(command.Id);

            if (account == null) throw new Exception("Account not found");

            account.UpdateBalance(command.Balance);            

            _unitOfWork.Account.UpdateAccount(account);

            await _unitOfWork.CommitAsync();
        }
    }
}
