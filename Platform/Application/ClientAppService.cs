﻿using Platform.Domain.Clients;
using Platform.Infrastructure.Helpers;
using System.Collections.Generic;

namespace Platform.Application
{
    public class ClientAppService : IClientAppService
    {
        public ClientAppService()
        {

        }

        public void SpecificationClient()
        {
            var clients = new List<Client>();
            clients.Add(new Client() { Name = "Test 1", Qualification = ClientQualification.Normal });
            clients.Add(new Client() { Name = "Test 2", Qualification = ClientQualification.Business });
            clients.Add(new Client() { Name = "Test 3", Qualification = ClientQualification.Premium });
            clients.Add(new Client() { Name = "Test 4", Qualification = ClientQualification.Top });
            clients.Add(new Client() { Name = "Test 5", Qualification = ClientQualification.Top });

            ISpecification<Client> clientTopSpec = new ExpressionSpecification<Client>(o => o.Qualification == ClientQualification.Top);

            var clientsTop = clients.FindAll(c => clientTopSpec.IsSatisfiedBy(c));
        }
    }
}
