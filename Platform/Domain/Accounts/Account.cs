﻿using Platform.Domain.Banks;
using System;

namespace Platform.Domain.Accounts
{
    public class Account //Entidade
    {
        public int Id { get; set; }        

        public decimal Balance { get; set; }

        public AccountType Type { get; set; }

        public int BankId { get; set; }

        public Bank Bank { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public Account()
        {
            CreatedAt = DateTimeOffset.Now;
        }

        public void UpdateBalance(decimal balance)
        {
            Balance = balance;
        }

        public void WithDraw(decimal value)
        {
            if (Balance < value)
                throw new Exception("The value is major then balance");

            Balance -= value;
        }
    }
}
