﻿using System;
using System.Threading.Tasks;

namespace Platform.Domain.Accounts
{
    public interface IAccountRepository : IDisposable
    {
        void CreateAccount(Account account);

        void DeactiveAccount(Account account);

        void UpdateAccount(Account account);

        Task<Account> GetAccountById(int accountId);
    }
}
