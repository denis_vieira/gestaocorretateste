﻿namespace Platform.Domain.Accounts
{
    public class AccountType // Objeto de Valor
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
