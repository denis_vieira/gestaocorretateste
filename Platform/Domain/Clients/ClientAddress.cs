﻿namespace Platform.Domain.Clients
{
    public class ClientAddress // Objeto de Valor
    {
        public Client Client { get; set; }

        public int ClientId { get; set; }

        public string Description { get; set; }

        public string Street { get; set; }

        public string Number { get; set; }
    }
}
