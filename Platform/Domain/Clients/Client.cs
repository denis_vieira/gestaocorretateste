﻿using System;
using System.Collections.Generic;

namespace Platform.Domain.Clients
{
    public class Client // Entidade
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ClientAccount> Accounts { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public ClientQualification Qualification { get; set; }

        public Client()
        {
            CreatedAt = DateTimeOffset.Now;
        }        
    }
}
