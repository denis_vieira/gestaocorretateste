﻿namespace Platform.Domain.Clients
{
    public enum ClientQualification
    {
        Normal,
        Premium, 
        Business,
        Top
    }
}
