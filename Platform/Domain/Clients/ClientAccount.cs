﻿using Platform.Domain.Accounts;

namespace Platform.Domain.Clients
{
    public class ClientAccount // Objeto de valor
    {
        public int Id { get; set; }

        public Account Account { get; set; }

        public int AccountId { get; set; }
    }
}
