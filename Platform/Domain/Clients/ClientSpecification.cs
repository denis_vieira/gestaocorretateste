﻿namespace Platform.Domain.Clients
{
    public class ClientSpecification
    {
        private ClientQualification Qualification;

        public ClientSpecification(ClientQualification qualification)
        {
            Qualification = qualification;
        }

        public bool IsSatisfiedBy(Client client)
        {
            return client.Qualification.Equals(Qualification);
        }
    }
}
