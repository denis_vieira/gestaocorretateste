﻿using Platform.Domain.Accounts;
using System.Collections.Generic;

namespace Platform.Domain.Banks
{
    public class Bank // Entidade
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Account> Accounts { get; set; }
    }
}
