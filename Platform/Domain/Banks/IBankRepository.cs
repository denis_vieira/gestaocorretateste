﻿using System.Threading.Tasks;

namespace Platform.Domain.Banks
{
    public interface IBankRepository
    {
         void CreateBank(Bank Bank);

        void DeactiveBank(Bank Bank);

        void UpdateBank(Bank Bank);

        Task<Bank> GetBankById(int bankId);
    }
}
