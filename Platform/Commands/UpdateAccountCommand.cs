﻿namespace Platform.Commands
{
    public class UpdateAccountCommand
    {
        public int BankId { get; internal set; }

        public decimal Balance { get; internal set; }

        public int Type { get; internal set; }

        public int Id { get; internal set; }
    }
}
