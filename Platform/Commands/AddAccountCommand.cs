﻿namespace Platform.Commands
{
    public class AddAccountCommand
    {
        public decimal Balance { get; internal set; }

        public int BankId { get; internal set; }

        public int Type { get; internal set; }
    }
}
