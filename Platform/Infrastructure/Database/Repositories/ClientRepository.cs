﻿using Platform.Domain.Clients;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Platform.Infrastructure.Database.Repositories
{
    public class ClientRepository
    {
        private readonly IDataSource _dataSource;
        private readonly AccountContext _context;

        public ClientRepository(IDataSource dataSource, AccountContext context)
        {
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void CreateClient(Client Client)
        {
            _context.Clients.Add(Client);
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        public async Task<Client> GetClientById(int ClientId)
        {
            return await _context.Clients.Where(c => c.Id == ClientId).FirstOrDefaultAsync();
        }

        public void DeactiveClient(Client Client)
        {
            _context.Clients.Remove(Client);
        }

        public void UpdateClient(Client Client)
        {
            _context.Entry(Client).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
