﻿using Platform.Domain.Accounts;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Platform.Infrastructure.Database.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IDataSource _dataSource;
        private readonly AccountContext _context;

        public AccountRepository(IDataSource dataSource, AccountContext context)
        {
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void CreateAccount(Account account)
        {
            _context.Accounts.Add(account);
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        public async Task<Account> GetAccountById(int accountId)
        {
            return await _context.Accounts.Where(c => c.Id == accountId).FirstOrDefaultAsync();
        }

        public void DeactiveAccount(Account account)
        {
            _context.Accounts.Remove(account);
        }

        public void UpdateAccount(Account account)
        {
            _context.Entry(account).State = System.Data.Entity.EntityState.Modified;            
        }
    }
}
