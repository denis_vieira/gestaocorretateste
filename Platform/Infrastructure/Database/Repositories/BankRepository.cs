﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Platform.Domain.Banks;

namespace Platform.Infrastructure.Database.Repositories
{
    public class BankRepository : IBankRepository
    {
        private readonly IDataSource _dataSource;
        private readonly AccountContext _context;

        public BankRepository(IDataSource dataSource, AccountContext context)
        {
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void CreateBank(Bank bank)
        {
            _context.Banks.Add(bank);
        }

        public void DeactiveBank(Bank bank)
        {
            _context.Banks.Remove(bank);
        }

        public async Task<Bank> GetBankById(int bankId)
        {
            return await _context.Banks.Where(c => c.Id == bankId).FirstOrDefaultAsync();
        }

        public void UpdateBank(Bank bank)
        {
            _context.Entry(bank).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
