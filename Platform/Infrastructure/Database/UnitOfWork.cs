﻿using Platform.Domain.Accounts;
using Platform.Infrastructure.Database.Repositories;
using System;
using System.Threading.Tasks;

namespace Platform.Infrastructure.Database
{
    public class UnitOfWork : IUnitOfWork
    {
        private IAccountRepository _account;
        private readonly AccountContext _accountContext;
        private readonly IDataSource _dataSource;

        public UnitOfWork(IDataSource dataSource, AccountContext accountContext)
        {
            _accountContext = accountContext ?? throw new ArgumentNullException(nameof(accountContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public IAccountRepository Account
        {
            get
            {
                if (_account == null)
                {
                    _account = new AccountRepository(_dataSource,_accountContext);
                }
                return _account;
            }
        }

        public async Task CommitAsync()
        {
            await _accountContext.SaveChangesAsync();
        }
    }
}
