﻿using System;
using System.Data.SqlClient;

namespace Platform.Infrastructure.Database
{
    public class DataSource : IDataSource
    {
        private readonly string connectionString = string.Empty;
        private SqlConnection sqlConnection;

        public DataSource(string connectionString)
        {
            this.connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public SqlConnection Connection()
        {
            if (sqlConnection == null)
            {
                sqlConnection = new SqlConnection(connectionString);
            }
            else if (sqlConnection != null && string.IsNullOrEmpty(sqlConnection.ConnectionString))
            {
                sqlConnection = new SqlConnection(connectionString);
            }

            return sqlConnection;
        }

        public string GetConnectionString()
        {
            return connectionString;
        }
    }
}
