﻿using System.Data.SqlClient;

namespace Platform.Infrastructure.Database
{
    public interface IDataSource
    {
        SqlConnection Connection();

        string GetConnectionString();
    }
}
