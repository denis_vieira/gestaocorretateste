﻿using Platform.Domain.Accounts;
using Platform.Domain.Banks;
using Platform.Domain.Clients;
using System.Data.Entity;

namespace Platform.Infrastructure.Database
{
    public class AccountContext : DbContext
    {
        public AccountContext(string connectionString) : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

          //  modelBuilder.Configurations.Add(new AccountMap());
          //  modelBuilder.Configurations.Add(new BankMap());
          //  modelBuilder.Configurations.Add(new ClientMap());
          //  modelBuilder.Configurations.Add(new ClientTypeMap());
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Client> Clients { get; set; }
    }
}
