﻿using Platform.Domain.Accounts;
using System.Threading.Tasks;

namespace Platform.Infrastructure.Database
{
    public interface IUnitOfWork
    {
        IAccountRepository Account { get; }

        Task CommitAsync();
    }
}
