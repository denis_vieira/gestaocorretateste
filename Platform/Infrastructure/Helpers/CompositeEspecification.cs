﻿namespace Platform.Infrastructure.Helpers
{
    public abstract class CompositeSpecification<T> : ISpecification<T>
    {
        public abstract bool IsSatisfiedBy(T o);
    }
}
