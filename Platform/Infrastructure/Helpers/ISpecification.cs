﻿namespace Platform.Infrastructure.Helpers
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T o);
    }
}
