﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Platform.Domain.Accounts;
using Platform.Infrastructure.Database;
using Platform.Infrastructure.Database.Repositories;
using System;

namespace Platform.Infrastructure.IOC
{
    public class BankModule
    {
        public BankModule(IConfiguration configuration) : this(new ServiceCollection(), configuration)
        {
        }

        public BankModule(IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            var config = configuration.GetSection("ConnectionStrings");
            var connectionString = config["TesteDb"];

            services.AddSingleton<IDataSource>(new DataSource(connectionString));
            services.AddSingleton<AccountContext>(new AccountContext(connectionString));
            services.AddTransient<IAccountRepository, AccountRepository>();
        }
    }
}
